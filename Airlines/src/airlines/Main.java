/**
* @author Anvar Hasanov
* 11-905
* Task #1 1k_22p
*/

package airlines;

import java.io.*;
import airlines.flight.Flight;

public class Main {
	public static void main(String[] args) throws IOException {

		String path = "C:/Users/Badmoon/Desktop/Git/hasanov_905/Airlines/";
		BufferedReader file = new BufferedReader(new FileReader(path + "flights.csv"));

		final int FLIGHTS_COUNT = 1200;
		Flight[] flights = new Flight[FLIGHTS_COUNT];
		String line = file.readLine();
		line = file.readLine();
		String[] arrayLine;

		for (int i = 0; i < FLIGHTS_COUNT; i++) {
			arrayLine = line.split(",");
			flights[i] = new Flight(arrayLine);
			line = file.readLine();
		}

		//// 	First item
		System.out.println("result = " + equalsFlight(flights));
		
		//// 	Second item
		System.out.println(equalsCountryFrom(flights));

		////    Third item
		System.out.println(equalsFlightNo(flights));


	}

	public static String equalsFlight(Flight[] flights) {
		int temp = flights[0].getAirline();
		int count = 1;
		int lastCount = 0;
		String result = "";
		// Because the airlines are already sorted in ascending order,
		// you can do without sorting the array
		for (int i = 1; i < flights.length; i++) {
			for ( ; i < flights.length && temp == flights[i].getAirline(); i++) {
				count++;
			}

			if (lastCount == count) {
				result += ", " + temp;
			}

			else if (lastCount < count) {
				result = temp + "";
			}

			lastCount = count;
			count = 1;
			if (i != flights.length) {
				temp = flights[i].getAirline(); 
			}
		}
		return result;
	}

	public static String equalsCountryFrom(Flight[] flights) {
		String[][] countryCont = new String[flights.length][2];
		countryCont[0][0] = flights[0].getSourceAirport();
		countryCont[0][1] = "1";
		int count = 1;
		boolean flag = false;
		for (int i = 0; i < flights.length; i++) {
			for (int j = 0; j < count; j++) {
				if (flights[i].getSourceAirport().equals(countryCont[j][0])) {
					countryCont[j][1] += "1";
					flag = true;
					break;
				}
			}

			if (!flag) {
				countryCont[count][0] = flights[i].getSourceAirport();
				countryCont[count][1] = "1";
				count++;
			}

			flag = false;
		}

		int maxCount = 0;
		String result = "";
		for (int i = 0; i < count; i++) {
			if (countryCont[i][1].length() > maxCount) {
				maxCount = countryCont[i][1].length();
				result = countryCont[i][0];
			}

			else if (countryCont[i][1].length() == maxCount) {
				result += ", " + countryCont[i][0];
			}
		}

		return result;
	}

	public static String equalsFlightNo(Flight[] flights) {
		
		String[] resultArray = new String[flights.length];
		int count = 0;
		int temp = 0;
		int airline = 0;
		boolean flag = false;
		for (int i = 0; i < flights.length; i++) {
			while (flights[i].getAirline() == flights[temp].getAirline() 
						&& temp < (flights.length - 1)) {
				temp++;
			}

			if (temp == (flights.length - 1)) {
				break;
			}

			for (int j = temp; j < flights.length; j++) {

				if (flights[i].getFlightNo() == flights[j].getFlightNo()) {
					for (int a = 0; a < count; a++) {
						if (resultArray[a].equals("(" + flights[i].getAirline() + ", " 
									+ flights[j].getAirline() + ")")) {
							flag = true;
							break;
						}
					}

					if (!flag) {
						resultArray[count] = "(" + flights[i].getAirline() + ", " 
								+ flights[j].getAirline() + ")";
						count++;
					}

					airline = flights[j].getAirline();

					while (flights[j].getAirline() == airline && j < (flights.length - 1)) {
						j++;
					}

					flag = false;

				}
			}
		}
		String result = "";
		for (int i = 0; i < count; i++) {
			result += resultArray[i] + " ";
		}

		return result;


	}


}