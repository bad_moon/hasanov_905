/**
* @author Anvar Hasanov
* 11-905
* Task #1 1k_22p
*/

package airlines.flight;

public class Flight {
	private int airline;
	private int flightNo;
	private String sourceAirport;
	private String destAirport;

	public Flight(String[] array) {
		this.airline = Integer.parseInt(array[0]);
		this.flightNo = Integer.parseInt(array[1]);
		this.sourceAirport = array[2];
		this.destAirport = array[3];
	}

	public int getAirline() {
		return this.airline;
	}

	public String getSourceAirport() {
		return this.sourceAirport;
	}

	public int getFlightNo() {
		return this.flightNo;
	}

	public String toString() {
		return "Airline No " + this.airline + " Fight No " + this.flightNo + " from "
				+ this.sourceAirport + " to " + this.destAirport;
	}
}