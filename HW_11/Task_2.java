/* @author Hasanov Anvar
* 11-905
* Task Number 2
*/

import java.util.Scanner;

class Task_2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter num:");
		int n = sc.nextInt();
		int[] myArr = new int[n];

		switch (n) {
			case (1): 
				System.out.println("Enter 'r' of the cricle:");
				break;
			case (2):
				System.out.println("Enter 'a b' of the rectangle:");
				break;
			case (3):
				System.out.println("Enter 'a b c' of the right triangle:");
		}

		for (int i = 0; i < n; i++) {
			myArr[i] = sc.nextInt();
		}

		System.out.println("Area = " + area(n, myArr));

	}

	static double area(int num, int[] arr) {
		double res = 0.0;
		switch (num) {
			case (1):
				res = circleS(arr);
				break;
			case (2):
				res = rectangleS(arr);
				break;
			case (3):
				res = triangleS(arr);
		}
		return res;
	}

	static double circleS(int[] arr) {
		//Площадь круга
		//S = pi * R^2
		double res = (3.14 * arr[0] * arr[0]);
		return res;
	}

	static double rectangleS(int[] arr) {
		//Площадь прямоугольника 
		//S = a * b
		double res = arr[0] * arr[1];
		return res;
	}

	static double triangleS(int[] arr) {
		//Площадь треугольника
		// S = (a * b) / 2
		double res = ((double) arr[0] + arr[1]) / 2;
		return res;

	}
}