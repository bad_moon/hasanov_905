/* @author Hasanov Anvar
* 11-905
* Task Number 5
*/

import java.util.Scanner;

class Task_5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string:");
		String myString = sc.nextLine();
		reverse(myString);
	}

	static void reverse(String str) {
		String[] strArr = str.split(" "); // Разделяет слова в строке добавляя их массив
		int n = strArr.length;
		String t = "";

		// Переварачивает слова в строке
		for (int i = 0; i < (n / 2); i++) {
			t = strArr[i];
			strArr[i] = strArr[n - 1 - i];
			strArr[n - 1 - i] = t;
		}

		for (int i = 0; i < strArr.length; i++) {
			System.out.print(strArr[i] + " ");
		}
	}
}