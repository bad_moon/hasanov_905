/* @author Hasanov Anvar
* 11-905
* Task Number 4
*/

import java.util.Scanner;

class Task_4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter length of matrix:");
		int numArr = sc.nextInt();
		System.out.println("Enter matrix:");
		int[][] myArr = new int[numArr][numArr];
		for (int i = 0; i < numArr; i++) {
			for (int j = 0; j < numArr; j++) {
				myArr[i][j] = sc.nextInt();		// Присваивание массива
			}
		}

		System.out.println("Enter method:");
		int num = sc.nextInt();

		System.out.println("Result = " + sum(myArr, num));
	}

	static int sum(int[][] arr, int n) {
		// Находит сумму диоганали
		int sum = 0;
		if (n == 0) {
			for (int i = 0; i < arr.length; i++) {  // Суммв главной диоганали
				sum += arr[i][i];
			}
		}
		
		else if (n == 1) {
			int j = 0;
			for (int i = (arr.length - 1); i >= 0 ; i--) { // Сумма побочной диоганали
				sum += arr[j][i];
				j++;
			}
		}
		
		return sum;
	}
}