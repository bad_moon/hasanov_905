/* @author Hasanov Anvar
* 11-905
* Task Number 3
*/

class Task_3 {
	public static void main(String[] args) {
		sum(args[0], args[1]);
	}

	static void sum(String num1 , String num2) {
		// Складывает два чиса из args
		int a = Integer.parseInt(num1);
		int b = Integer.parseInt(num2);
		System.out.println(a + b);
	}
}