/* @author Hasanov Anvar
* 11-905
* Task Number 1
*/

import java.util.Scanner;

class Task_1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the length of the array:");
		int num = sc.nextInt();
		int[] myArr = new int[num];
		System.out.println("Enter the array:");
		for (int i = 0; i < num; i++) {
			myArr[i] = sc.nextInt();
		}
		
		System.out.println("Average = " + avg(myArr));
	}

	static double avg(int[] arr) {
		// Среднее арифметическое чисел
		double res = 0;
		int n = arr.length;
		for (int i = 0; i < n; i++) {
			res += arr[i];
		}
		res /= n;
		return res;
	}
}