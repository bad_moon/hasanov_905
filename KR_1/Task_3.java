/* Hasanov Anvar
11-905
Task 2
*/

import java.util.Scanner;

class Task_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int arr[] = new int[n];
        // Создается массив
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        int arrInt = 0;
        // Элементы массива меняются местами
        for (int i = 0; i < n / 2; i++) {
            arrInt = arr[i];
            arr[i] = arr[(n - 1) - i];
            arr[(n - 1) - i] = arrInt;
        }
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i] + " ");
        }

    }
}