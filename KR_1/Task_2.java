/* Hasanov Anvar
11-905
Task 2
*/

import java.util.Scanner;

class Task_2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		int n = sc.nextInt();

		double res = (double) ((x * x * x) / 2);

		double copX = 1;
		double del = 1;
		//Cумаа
		for (int i = 2; i <= n; i++) {
			// Степень Х
			for (int j = 1; j <= i + 2; j++) {
				copX *= x;
			}
			//Факториал
			for (int j = 2; j <= 2 * i; j++) {
				del *= j; 
			}
			res += (double) ((copX * i) / del);
			copX = 1;
			del = 1;
		}
		System.out.println(res);
	}
}