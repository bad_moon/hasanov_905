/* Hasanov Anvar
11-905
Task 1
*/

import java.util.Scanner;

class Task_1 {
    public static void main(String[] args) {
        String num1 = "June";
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите месяц");
        String mon = sc.nextLine();
        System.out.println("Введите день");
        int day = sc.nextInt();

        // equals сравнивает две строки
        if (mon.equals("January")) {
            if (day <= 19) {
                System.out.println("Козерог");
            } else {
                System.out.println("Водолей");
            }
        } else if (mon.equals("February")) {
            if (day <= 18) {
                System.out.println("Водолей");
            } else {
                System.out.println("Рыба");
            }
        } else if (mon.equals("March")) {
            if (day <= 20) {
                System.out.println("Рыба");
            } else {
                System.out.println("Овен");
            }
        } else if (mon.equals("Apri")) {
            if (day <= 19) {
                System.out.println("Овен");
            } else {
                System.out.println("Телец");
            }
        } else if (mon.equals("May")) {
            if (day <= 20) {
                System.out.println("Телец");
            } else {
                System.out.println("Близнец");
            }
        } else if (mon.equals("June")) {
            if (day <= 20) {
                System.out.println("Близнец");
            } else {
                System.out.println("Рак");
            }
        } else if (mon.equals("July")) {
            if (day <= 22) {
                System.out.println("Рак");
            } else {
                System.out.println("Лев");
            }
        } else if (mon.equals("Augus")) {
            if (day <= 22) {
                System.out.println("Лев");
            } else {
                System.out.println("Дева");
            }
        } else if (mon.equals("September")) {
            if (day <= 22) {
                System.out.println("Дева");
            } else {
                System.out.println("Весы");
            }
        } else if (mon.equals("October")) {
            if (day <= 22) {
                System.out.println("Весы");
            } else {
                System.out.println("Скорпион");
            }
        } else if (mon.equals("November")) {
            if (day <= 21) {
                System.out.println("Скорпион");
            } else {
                System.out.println("Стрелец");
            }
        } else if (mon.equals("December")) {
            if (day <= 21) {
                System.out.println("Стрелец");
            } else {
                System.out.println("Козерог");
            }
        }
    }
}