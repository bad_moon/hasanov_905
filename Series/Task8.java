/**
* @author Anvar Hasanov
* 11-905
* Task Number 8
*/

import java.util.Scanner;

class Task8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double res = 1.0;
		double resLast = 0.0;
		int ver = 1;
		long del = 1;
		double eps = 0.001;

		for (int k = 1; (k <= n) && (res - resLast > eps); k++) {
			resLast = res;
			ver *= k;
			del = power(del, k);
			res += (double) (ver * ver) / del;
		}

		res = Math.round(res * 100.0) / 100.0;
		System.out.println(res);

	}

	static long power(long num, long x) {
		for (int i = 1; i <= x * x - (x - 1) * (x - 1); i++) {  // Cтепень двойки считается не с нуля!!!
			num *= 2;											// The power of two is not considered from scratch !!!
		}
		return num;
	}
}