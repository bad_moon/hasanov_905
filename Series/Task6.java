/**
* @author Anvar Hasanov
* 11-905
* Task Number 6
*/

import java.util.Scanner;

class Task6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double res = 1.0;
		double resLast = 0.0;
		int ver = 1;
		long del = 1;
		double eps = 0.001;

		for (int k = 1; (k <= n) && (res - resLast > eps); k++) {
			resLast = res;
			ver *= k;
			del *= (2 * k) * (2 * k - 1);
			res += (double) (ver * ver) / del;
		}

		res = Math.round(res * 100.0) / 100.0;
		System.out.println(res);
	} 
}