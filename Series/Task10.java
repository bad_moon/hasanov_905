/**
* @author Anvar Hasanov
* 11-905
* Task Number 10
*/

import java.util.Scanner;

class Task10 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double res = 0.0;
		double ver = 1.0;

		for (long k = 1; k <= n; k++) {
			ver *= 2.7182 * k;					// Число е взял равное 2.7182
			res += (double) ver / power(k);
		}

		res = Math.round(res * 100.0) / 100.0;
		System.out.println(res);
	}

	static long power(long x) {
		long p = x;
		for (int i = 2; i <= p; i++) {
			x *= p;
		}
		return x;

	}
}