/**
* @author Anvar Hasanov
* 11-905
* Task Number 1
*/

import java.util.Scanner;

class Task1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int x = sc.nextInt();
		int copX = x;			// Копия Х для подсчета степени
		int res = 0;

		for (int k = 1; k <= n; k++) {
			res += k * copX;	
			copX *= x;
		}

		System.out.println(res * 1.0); // "... выдавать результат в виде числа с плавающей тоской ...""
	}
}