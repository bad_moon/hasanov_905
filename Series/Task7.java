/**
* @author Anvar Hasanov
* 11-905
* Task Number 7
*/

import java.util.Scanner;	

class Task7 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double res = 0.0;
		double resLast = -1.0;
		int ver = 1;
		double eps = 0.001;

		for (long k = 1; (k <= n) && (res - resLast > eps); k++) {  // long нужен для метода т.к число может выйти за пределы int
			resLast = res;
			ver *= 2 * k;
			res += (double) ver / power(k);
		}

		res = Math.round(res * 100.0) / 100.0;
		System.out.println(res);
	}

	static long power(long x) {
		long p = x;
		for (int i = 2; i <= p; i++) {
			x *= p;
		}
		return x;
	}	
}