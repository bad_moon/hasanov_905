/**
* @author Anvar Hasanov
* 11-905
* Task Number 4
*/

import java.util.Scanner;

class Task4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double res = 0.0;

		for (int k = 1; k <= n; k++) {
			res += (2.0 * k + 1.0) / (k * k * (k + 1) * (k + 1));
		}

		res = Math.round(res * 100.0) / 100.0;
		System.out.println(res);

	}
}