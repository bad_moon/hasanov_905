/**
* @author Anvar Hasanov
* 11-905
* Task Number 5
*/

import java.util.Scanner;

class Task5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int res = 0;

		for (long k = 1; k <= n; k++) {			// if (n % 2 == 1) {
			if (k % 2 == 1) {					// 	   res = 1;
				res += 1;						// }
			}									// else {
			else {								//     res = 0;
				res += -1;						// }
			}									// Можно было обойтись и без цикла, но это
		}										// совсем удовлетворяет требованиям задачи

		System.out.println(res * 1.0);
	}
}