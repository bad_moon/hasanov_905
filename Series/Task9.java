/**
* @author Anvar Hasanov
* 11-905
* Task Number 9
*/

import java.util.Scanner;

class Task9 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double res = 0.0;
		double resLast = -1.0;
		int del = 1;
		double eps = 0.001;

		for (int k = 1; (k <= n) && (res - resLast > eps); k++) {
			resLast = res;
			del *= 2;
			if (k % 2 == 0) {
				res += 3.0 / del;
			}
			else {
				res += 1.0 / del;
			}
		}

		res = Math.round(res * 100.0) / 100.0;
		System.out.println(res);

	}
}