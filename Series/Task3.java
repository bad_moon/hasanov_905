/**
* @author Anvar Hasanov
* 11-905
* Task Number 3
*/

import java.util.Scanner;

class Task3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double num1 = 1.0;
		double num2 = 1.0;
		double num2Sum = 1.0;
		double res = 1.0;
		double eps = 0.001;

		for (int k = 1; (k <= n) && (num1 > eps); k++) { // Потому что num1 стремиться к нулю медленнее 
			num1 *= 0.8;
			res += num1;	

			num2 *= 0.3;
			num2Sum += num2;
		}
		
		res = res - 3.0 * num2Sum;
		res = Math.round(res * 100.0) / 100.0;

		System.out.println(res);
	}
}