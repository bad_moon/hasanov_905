/**
* @author Anvar Hasanov
* 11-905
* Task Number 2
*/

import java.util.Scanner;

class Task2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double res = 1.0;
		double resLast = 0.0;
		int del = 1;
		int num = 1;
		double eps = 0.001;
		/*
		 * Мы считаем число с точьностью до 2 знаков после запятой,
		 * поэтому нет смысла продолжать считать, если результат
		 * увеличивается меньше чем на 0.001
		*/
		
		for (int k = 1; (k <= n) && (res - resLast > eps); k++) {   // Если результат больше предидущего меньше чем на 0.001,
			resLast = res;							                // то цикл прекращает работу
			del *= 4.0 * k * (k + 1);		// (2 * k + 1) * (2 * k)
			num *= 3;
			res += (double) num / del;
		}

		res = Math.round(res * 100.0) / 100.0;
		System.out.println(res);

	}
}