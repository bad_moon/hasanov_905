import java.util.Scanner;

class Num11 {
    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        int nm = a.nextInt();
        String o = "0";
        for (int i = 1; nm >= i; i++) {
            for (int e = 1; e <= i; e++) System.out.print(o);
            System.out.print("\n");
        }
        
    }
}

/**
* @author Anvar Hasanov
* 11-905
* Task Number 11
*/