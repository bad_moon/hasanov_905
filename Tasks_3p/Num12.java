import java.util.Scanner;

class Num12 {
    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        int n1 = a.nextInt();

        Scanner b = new Scanner(System.in);
        int n2 = b.nextInt();
        int as = 0;

        if (n1 > n2) {
            as = n1;
            n1 = n2;
            n2 = as;
        }

        if (n1 < n2) {
            for ( ; n1 <= n2; n1++) {
                if (n1 % 3 == 0) System.out.print(n1 + " ");
            }
        }

    }
}

/**
* @author Anvar Hasanov
* 11-905
* Task Number 12
*/