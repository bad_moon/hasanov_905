import java.util.Scanner;

class Num7 {
    public static void main(String[] args) {
        System.out.print(" Введите номер месяца от 1 до 6 ---> ");
        Scanner a = new Scanner(System.in);
        int nmb = a.nextInt();
        if (nmb == 1) System.out.println("Январь");
        else if (nmb == 2) System.out.println(" Февраль ");
        else if (nmb == 3) System.out.println(" Март ");
        else if (nmb == 4) System.out.println(" Апрель ");
        else if (nmb == 5) System.out.println(" Май ");
        else if (nmb == 6) System.out.println(" Июнь ");
        else System.out.println(" Не верный номер ");
    }
}

/**
* @author Anvar Hasanov
* 11-905
* Task Number 7
*/