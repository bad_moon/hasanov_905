class Num9 {
    public static void main(String[] args) {
        int res = 0;
        for (int i = 1; i < 37; i += 2) res += i;
        System.out.println(res);
    }
}

/**
* @author Anvar Hasanov
* 11-905
* Task Number 9
*/