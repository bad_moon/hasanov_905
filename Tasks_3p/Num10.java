class Num10 {
    public static void main(String[] args) {
       // int a = 1, b = 2;
        double i = 0, a = 1, b = 2;
        while (a != 100) {
            i += (a / b);
            a++;
            b++;
        }
        System.out.println(i);

    }
}

/**
* @author Anvar Hasanov
* 11-905
* Task Number 10
*/