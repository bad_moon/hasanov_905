import java.util.Scanner;

class Num4 {
    public static void main(String[] args) {
		System.out.print("Введите любую строку ---> ");
		Scanner a = new Scanner(System.in);
		String str = a.nextLine();
		String rev = new StringBuffer(str).reverse().toString();
		System.out.println ("Строка в обратном порядке, после реверса ---> " + rev);
	}
}

// Без помощи гугла не обошлось 

/**
* @author Anvar Hasanov
* 11-905
* Task Number 4
*/