import java.util.Scanner;

class Num3 {
    public static void main(String[] args){
        System.out.print("Введите пятизначное число ---> ");
        Scanner ns = new Scanner(System.in);
        int a = ns.nextInt();
        int x = 0, y = 0, res1 = 0, res2 = 0;
        if (a / 10000 >= 1 || a / 10000 <= -1) {
            if (a < 0) a *= -1;
            for (int i = 1; a > 0; i++) {
                if (i % 2 == 0) res1 = res1 + (a % 10);
                else res2 = res2 + (a % 10);
                a /= 10;
            }
        System.out.println(" Cумма чисел на четных местах = "+  res1 + "\n Сумма чисел на нечетных местах = " + res2);
        }
        else System.out.println("Error \nЧисло не пятизначное");
    }
}

/**
* @author Anvar Hasanov
* 11-905
* Task Number 3
*/