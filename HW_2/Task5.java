/**
* @author Anvar Hasanov
* 11-905
* Task Number 5
*/

/*
* I'm an idiot
* I didn’t understand the task correctly, 
* so the code is uploaded a day later 
* than the deadline
*/

import java.util.Scanner;

class Task5 {
	public static void main(String[] args) {  
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();					// input n
		int max = n, min = n;					// n is assigned max and min
		int cm = 0, res = 0; 
		n = sc.nextInt();
		while (n != 0) {
			if (n > max) {							
				max = n;				// n is assigned max
				cm = 0;
				n = sc.nextInt();
				continue;
			}
			else if (n <= min) {        // Only changed <=
				min = n;				// n is assigned min
				res = cm;
			}
			cm += n;
			n = sc.nextInt();
		}	
		System.out.println("\n" + res);
	}
}