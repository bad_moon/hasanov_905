/**
* @author Anvar Hasanov
* 11-905
* Task Number 9
*/

import java.util.Scanner;

class Task9 {
	public static void main(String[] args) {
		System.out.println("Enter n");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		for(int stb = 1; stb <= (n / 2); stb++) {    // For the first half of matrix
			for(int str = 1; str <= stb; str++) { 
				System.out.print(stb + " ");
			}
			for(int str = stb; str < (n / 2); str++) { 
				System.out.print(str + 1 + " ");
			}
			for(int str = (n / 2) + 1; str <= n; str++) {
				System.out.print(str - 1 + stb + " ");
			}
			System.out.println();
		}
		for(int stb = (n / 2) + 1; stb <= n; stb++) {  // For the second half of matrix
			for(int str = 1; str <= (n / 2); str++) {
				System.out.print(str - 1 + stb + " ");
			}
			for(int str = (n / 2) + 1; str < stb; str++) {
				System.out.print(str + " ");
			}
			for(int str = stb; str <= n; str++) {
				System.out.print(stb + " ");
			}
			System.out.println();
		}
	}
}