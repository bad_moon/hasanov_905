/**
* @author Anvar Hasanov
* 11-905
* Task Number 7
*/

import java.util.Scanner;

class Task7 {
	public static void main(String[] args) {
		System.out.println("Enter n");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		for(int stb = 1; stb <= n; stb++) {
			for(int str = 1; str <= n; str++) {
				System.out.print(n - str +stb + " "); //Just as easy
			}
			System.out.println();
		}
	}
}