/**
* @author Anvar Hasanov
* 11-905
* Task Number 3
*/

import java.util.Scanner;

class Task3 {
	public static void main(String[] args) {
		System.out.println("Enter");
		Scanner sc = new Scanner(System.in);
		int n = 1, x = 0;
		x = sc.nextInt();
		while(x != 0) {
			n = sc.nextInt();		
			if (x > n) {
				x = n;		// comparison
				break;
			}
			x = n;
		}
		while(x != 0) {
			n = sc.nextInt();
			if (x < n && n != 0) {
				System.out.println("Error");
				break;
			}
			x = n;
		}
		if (n == 0) {
			System.out.println("Right");
		}
	}
}