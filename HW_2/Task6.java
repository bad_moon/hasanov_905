/**
* @author Anvar Hasanov
* 11-905
* Task Number 6
*/

import java.util.Scanner;

class Task6 {
	public static void main(String[] args) {
		System.out.println("Enter n");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();;
		for(int stb = 1; stb <= n; stb++) {
			for(int str = 0; str < n; str++) {
				System.out.print(stb + str + " "); // It's easy
			}
			System.out.println();
		}
	}
}