/**
* @author Anvar Hasanov
* 11-905
* Task Number 8
*/

import java.util.Scanner;

class Task8 {
	public static void main(String[] args) {
		System.out.println("Enter n");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		for(int stb = 1; stb <= n; stb++) {
			for(int str = 1; str < stb; str++) {
				System.out.print(stb + " ");        //Just as easy
			}
			for(int str = stb; str <= n; str++) {
				System.out.print(str + " ");
			}
			System.out.println();
		}
	}
}