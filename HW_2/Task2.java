/**
* @author Anvar Hasanov
* 11-905
* Task Number 2
*/

/**
* Nado socratit do i < sqrt(x)
*/

import java.util.Scanner;

class Task2 {
	public static void main(String[] args) {
		System.out.println("Enter");
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt(); 						//Write X
		int i = 2;
		for(; i < x; i++) {
			if ( x % i == 0 ) {					//Is calculated
			break;
			}
		}
		if (x == i) {
			System.out.println("Prostoe");
		}
		else {
			System.out.println("Ne Prostoe");
		}
	}
}