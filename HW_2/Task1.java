/**
* @author Anvar Hasanov
* 11-905
* Task Number 1
*/

class Task1 {
    public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);		// Write X and N
		int x1 = 1, k1 = 1;
		double res = 0.0;
		for(int k = 1; k <= n; k++) {
			k1 = 1;
			x1 = 1;
			for(int i = 1; i <= (3 * k); i++) {		// Is calculated X
				x1 *= x;
			}
			for(int i = 1; i <= (2 * k); i++) {		//Is calculated K
				k1 *= k;
			}
			res += (double) x1 / k1;		//Is calculated
		}
        System.out.println(res);
    }
}