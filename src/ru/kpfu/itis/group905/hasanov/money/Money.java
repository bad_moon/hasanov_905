/**
* @author Anvar Hasanov
* 11-905
* Task #1
*/

package ru.kpfu.itis.group905.hasanov.money;
import ru.kpfu.itis.group905.hasanov.basicClasses.RationalFraction;

public class Money {
	private int rubles;
	private short pennies;

	public Money() {
		this(0, (short) 0);
	}

	public Money(int rubles, short pennies) {
		this.rubles = rubles;
		this.pennies = pennies;

		while (this.pennies <= -100) {
				this.rubles -= 1;
				this.pennies += 100;
		}

		while (this.pennies >= 100) {
				this.rubles += 1;
				this.pennies -= 100;
		}

		if (this.rubles < 0) {
			if (this.pennies > 0) {
				this.rubles += 1;
				this.pennies -= 100;
			}
		}

		else if (this.rubles > 0) {
			if (this.pennies < 0) {
				this.rubles -= 1;
				this.pennies += 100;
			}
		}
	}

	public Money add(Money secondMoney) {
		return new Money(this.rubles + secondMoney.rubles,
				(short)(this.pennies + secondMoney.pennies)); 
	}

	public Money sub(Money secondMoney) {
		return new Money(this.rubles - secondMoney.rubles,
				(short)(this.pennies - secondMoney.pennies));
	}

	public double div(Money secondMoney) {
		return (this.rubles + (this.pennies / 100.0)) 
				/ (secondMoney.rubles + (secondMoney.pennies / 100.0));
	}

	public Money divRational(RationalFraction rational) {
		RationalFraction result = 
				new RationalFraction(this.rubles * 100 + (int)this.pennies, 100);                 ////return Money
		double doubleMoney = result.div(rational).value();
		return new Money((int)doubleMoney, (short)((doubleMoney % 1) * 100));
	}

	public Money multRational(RationalFraction rational) {
		RationalFraction result = 
				new RationalFraction(this.rubles * 100 + (int)this.pennies, 100);                 ////return Money
		double doubleMoney = result.mult(rational).value();
		return new Money((int)doubleMoney, (short)((doubleMoney % 1) * 100));
	}

	public boolean equals(Money secondMoney) {
		if (this.rubles == secondMoney.rubles 
				&& this.pennies == secondMoney.pennies) {
			return true;
		}
		return false;
	}

	public String toString() {
		if (this.rubles < 0 && this.pennies < 0) {
			return this.rubles + "," + this.pennies * (-1);
		}
		else if (this.rubles == 0 && this.pennies < 0) {
			return "-" + this.rubles + "," + this.pennies * (-1);
		}
		return this.rubles + "," + this.pennies;
	}

	public void setRubles(int rubles) {
		this.rubles = rubles;
	}

	public void setPennies(short pennies) {
		this.pennies = pennies;
	}

	public int getRubels() {
		return this.rubles;
	}

	public short getPennies() {
		return this.pennies;
	}
}