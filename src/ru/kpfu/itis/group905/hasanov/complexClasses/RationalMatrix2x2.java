/*
* @author Anvar Hasanov
* 11-905
* Task #3
*/

package ru.kpfu.itis.group905.hasanov.complexClasses;

import ru.kpfu.itis.group905.hasanov.basicClasses.RationalFraction;

public class RationalMatrix2x2 {
	private RationalFraction[][] rationalMatrix = new RationalFraction[2][2];

	public RationalMatrix2x2() {
		this(new RationalFraction());
	}

	public RationalMatrix2x2(RationalFraction number) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				rationalMatrix[i][j] = number;
			}
		}
	}

	public RationalMatrix2x2(RationalFraction firstNumber, 
			RationalFraction secondNumber, RationalFraction thirdNumber,
			RationalFraction fourthNumber) {
		rationalMatrix[0][0] = firstNumber;
		rationalMatrix[0][1] = secondNumber;
		rationalMatrix[1][0] = thirdNumber;
		rationalMatrix[1][1] = fourthNumber;
	}

	public RationalMatrix2x2 add(RationalMatrix2x2 secondRationalMatrix) {
		RationalMatrix2x2 result = new RationalMatrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.rationalMatrix[i][j] = this.rationalMatrix[i][j].add(
						secondRationalMatrix.rationalMatrix[i][j]);
			}
		}
		return result;
	}

	public RationalMatrix2x2 mult(RationalMatrix2x2 secondRationalMatrix) {
		RationalMatrix2x2 result = new RationalMatrix2x2();
		RationalFraction firstTemp = new RationalFraction();
		RationalFraction secondTemp = new RationalFraction();

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				firstTemp = this.rationalMatrix[i][0].mult(
						secondRationalMatrix.rationalMatrix[0][j]);
				secondTemp = this.rationalMatrix[i][1].mult(
						secondRationalMatrix.rationalMatrix[1][j]);
				result.rationalMatrix[i][j] = firstTemp.add(secondTemp);
			}
		}
		return result;
	}

	public RationalFraction det() {
		RationalFraction firstTemp = new RationalFraction();
		RationalFraction secondTemp = new RationalFraction();
		firstTemp = this.rationalMatrix[0][0].mult(this.rationalMatrix[1][1]);
		secondTemp = this.rationalMatrix[0][1].mult(this.rationalMatrix[1][0]);
		return firstTemp.sub(secondTemp);
	} 

	public RationalVector2D multVector(RationalVector2D rationalVector) {
		RationalFraction firstTemp = new RationalFraction();
		RationalFraction secondTemp = new RationalFraction();
		RationalFraction thirdTemp = new RationalFraction();
		firstTemp = this.rationalMatrix[0][0].mult(rationalVector.getX());
		secondTemp = this.rationalMatrix[0][1].mult(rationalVector.getY());
		firstTemp = firstTemp.add(secondTemp);
		secondTemp = this.rationalMatrix[1][0].mult(rationalVector.getX());
		thirdTemp = this.rationalMatrix[1][1].mult(rationalVector.getY());
		secondTemp = secondTemp.add(thirdTemp);

		return new RationalVector2D(firstTemp, secondTemp);
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				str += this.rationalMatrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}
}