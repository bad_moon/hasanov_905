/*
* @author Anvar Hasanov
* 11-905
* Task #1
*/

package ru.kpfu.itis.group905.hasanov.complexClasses;

import ru.kpfu.itis.group905.hasanov.basicClasses.RationalFraction;

public class RationalVector2D {
    private RationalFraction x;
    private RationalFraction y;

    public RationalVector2D() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalVector2D(RationalFraction x, RationalFraction y) {
        this.x = x;
        this.y = y;
    }

    public RationalVector2D add(RationalVector2D secondVector) {
        return new RationalVector2D(this.x.add(secondVector.x), 
                this.y.add(secondVector.y));
    }

    public String toString() {
        return "{" + this.x + "; " + this.y + "}";
    }

    public double length() {
        RationalFraction resultFaction = this.x.mult(this.x);
        resultFaction.add2(this.y.mult(this.y));
        return Math.sqrt((double) resultFaction.getNumerator() 
                / resultFaction.getDenominator());
    }

    public RationalFraction scalarProduct(RationalVector2D secondVector) {
        RationalFraction result = this.x.mult(secondVector.x);
        result.add2(this.y.mult(secondVector.y));
        return result;

    }

    public boolean equals(RationalVector2D secondVector) {
        if (this.x.equals(secondVector.x) && this.y.equals(secondVector.y)) {
            return true;
        }
        return false;
    }

    public RationalFraction getX() {
        return this.x;
    }

    public RationalFraction getY() {
        return this.y;
    }
}