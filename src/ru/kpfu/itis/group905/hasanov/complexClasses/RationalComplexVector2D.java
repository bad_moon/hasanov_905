/**
* @author Anvar Hasanov
* 11-905
* Task #6
*/

package ru.kpfu.itis.group905.hasanov.complexClasses;

import ru.kpfu.itis.group905.hasanov.complexClasses.RationalComplexNumber;

public class RationalComplexVector2D  {
	private RationalComplexNumber x;
	private RationalComplexNumber y;

	public RationalComplexVector2D() {
        this(new RationalComplexNumber(), new RationalComplexNumber());
    }

    public RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public RationalComplexVector2D add(RationalComplexVector2D secondVector) {
    	return new RationalComplexVector2D(this.x.add(secondVector.x),
    			this.y.add(secondVector.y));
    }

    public RationalComplexNumber scalarProduct(
    		RationalComplexVector2D secondVector) {  							 // Task with *
        RationalComplexNumber result = this.x.mult(secondVector.x);
        return result.add(this.y.mult(secondVector.y));
    }

    public String toString() {
        return "{" + this.x + "; " + this.y + "}";
    }

    public RationalComplexNumber getX() {
        return this.x;
    }

    public RationalComplexNumber getY() {
        return this.y;
    }


}