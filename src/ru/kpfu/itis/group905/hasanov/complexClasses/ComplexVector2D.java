/*
* @author Anvar Hasanov
* 11-905
* Task #2
*/

package ru.kpfu.itis.group905.hasanov.complexClasses;

import ru.kpfu.itis.group905.hasanov.basicClasses.ComplexNumber;

public class ComplexVector2D {
	private ComplexNumber x;
	private ComplexNumber y;

	public ComplexVector2D() {
        this(new ComplexNumber(), new ComplexNumber());
    }

    public ComplexVector2D(ComplexNumber x, ComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public ComplexVector2D add(ComplexVector2D secondVector) {
    	return new ComplexVector2D(this.x.add(secondVector.x),
    			this.y.add(secondVector.y));
    }

    public String toString() {
        return "{" + this.x + "; " + this.y + "}";
    }

    public boolean equals(ComplexVector2D secondVector) {
    	if (this.x.equals(secondVector.x) && this.y.equals(secondVector.y)) {
    		return true;
    	}
    	return false;
    }

    public ComplexNumber scalarProduct(ComplexVector2D secondVector) {   // Task with *
        ComplexNumber result = this.x.mult(secondVector.x);
        return result.add(this.y.mult(secondVector.y));
    }

    public ComplexNumber getX() {
        return this.x;
    }

    public ComplexNumber getY() {
        return this.y;
    }
}