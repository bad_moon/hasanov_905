/*
* @author Anvar Hasanov
* 11-905
* Task #5
*/

package ru.kpfu.itis.group905.hasanov.complexClasses;

import ru.kpfu.itis.group905.hasanov.basicClasses.RationalFraction;

public class RationalComplexNumber {
	private RationalFraction realNumber;
	private RationalFraction imaginaryNumber;

	public RationalComplexNumber() {
		this(new RationalFraction(), new RationalFraction());
	}

	public RationalComplexNumber(RationalFraction realNumber,
			RationalFraction imaginaryNumber) {
		this.realNumber = realNumber;
		this.imaginaryNumber = imaginaryNumber;
	}

	public RationalComplexNumber add(RationalComplexNumber secondNumber) {
		return new RationalComplexNumber(
				this.realNumber.add(secondNumber.realNumber),
				this.imaginaryNumber.add(secondNumber.imaginaryNumber));
	}

	public RationalComplexNumber sub(RationalComplexNumber secondNumber) {
		return new RationalComplexNumber(
				this.realNumber.sub(secondNumber.realNumber),
				this.imaginaryNumber.sub(secondNumber.imaginaryNumber));
	}

	public RationalComplexNumber mult(RationalComplexNumber secondNumber) {
		RationalFraction newReal = this.realNumber.mult(secondNumber.realNumber);
		RationalFraction temp =
				this.imaginaryNumber.mult(secondNumber.imaginaryNumber);
		newReal.add2(temp.mult(new RationalFraction(-1, 1)));
		RationalFraction newImaginary =
				this.imaginaryNumber.mult(secondNumber.realNumber);
		newImaginary.add2(this.realNumber.mult(secondNumber.imaginaryNumber));

		return new RationalComplexNumber(newReal, newImaginary);
	}

	public String toString() {
		if (realNumber.getNumerator() == 0) {
			if (imaginaryNumber.getNumerator() == 0) {
				return "0";
			}
		}
		if (imaginaryNumber.getNumerator() == 0) {
			return realNumber + "";
		}
		if (imaginaryNumber.getNumerator() < 0) {
			RationalFraction newImaginaryNumber = new RationalFraction(
					imaginaryNumber.getNumerator(), imaginaryNumber.getDenominator());
			newImaginaryNumber.setNumerator(newImaginaryNumber.getNumerator() * (-1));
			return realNumber + " - " + newImaginaryNumber + "i";
		}
		return realNumber + " + " + imaginaryNumber + "i";
	}
}