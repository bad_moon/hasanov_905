/*
* @author Anvar Hasanov
* 11-905
* Task #4
*/

package ru.kpfu.itis.group905.hasanov.complexClasses;

import ru.kpfu.itis.group905.hasanov.basicClasses.ComplexNumber;

public class ComplexMatrix2x2 {
	private ComplexNumber[][] matrix = new ComplexNumber[2][2];

	public ComplexMatrix2x2() {
		this(new ComplexNumber());
	}

	public ComplexMatrix2x2(ComplexNumber number) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				matrix[i][j] = number;
			}
		}
	}

	public ComplexMatrix2x2(ComplexNumber firstNumber, ComplexNumber secondNumber, 
			ComplexNumber thirdNumber, ComplexNumber fourthNumber) {
		matrix[0][0] = firstNumber;
		matrix[0][1] = secondNumber;
		matrix[1][0] = thirdNumber;
		matrix[1][1] = fourthNumber;
	}

	public ComplexMatrix2x2 add(ComplexMatrix2x2 secondMatrix) {
		ComplexMatrix2x2 result = new ComplexMatrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][j].add(secondMatrix.matrix[i][j]);
			}
		}
		return result;
	}

	public ComplexMatrix2x2 mult(ComplexMatrix2x2 secondMatrix) {
		ComplexMatrix2x2 result = new ComplexMatrix2x2();
		ComplexNumber firstTemp = new ComplexNumber();
		ComplexNumber secondTemp = new ComplexNumber();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				firstTemp = this.matrix[i][0].mult(secondMatrix.matrix[0][j]);
				secondTemp = this.matrix[i][1].mult(secondMatrix.matrix[1][j]);
				result.matrix[i][j] = firstTemp.add(secondTemp);
			}
		}
		return result;
	}

	public ComplexVector2D multVector(ComplexVector2D complexVector) {
		ComplexNumber firstTemp = new ComplexNumber();
		ComplexNumber secondTemp = new ComplexNumber();
		ComplexNumber thirdTemp = new ComplexNumber();
		firstTemp = this.matrix[0][0].mult(complexVector.getX());
		secondTemp = this.matrix[0][1].mult(complexVector.getY());
		firstTemp = firstTemp.add(secondTemp);
		secondTemp = this.matrix[1][0].mult(complexVector.getX());
		thirdTemp = this.matrix[1][1].mult(complexVector.getY());
		secondTemp = secondTemp.add(thirdTemp);

		return new ComplexVector2D(firstTemp, secondTemp);
	}

	public ComplexNumber det() {
		ComplexNumber result = this.matrix[0][0].mult(this.matrix[1][1]); // Task with *
		return result.sub(this.matrix[0][1].mult(this.matrix[1][0]));
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				str += this.matrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}
}