/**
* @author Anvar Hasanov
* 11-905
* Task #7
*/

package ru.kpfu.itis.group905.hasanov.complexClasses;

import ru.kpfu.itis.group905.hasanov.complexClasses.RationalComplexNumber;

public class RationalComplexMatrix2x2 {
	private  RationalComplexNumber[][] matrix = new  RationalComplexNumber[2][2];

	public RationalComplexMatrix2x2() {
		this(new  RationalComplexNumber());
	}

	public RationalComplexMatrix2x2( RationalComplexNumber number) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				matrix[i][j] = number;
			}
		}
	}

	public RationalComplexMatrix2x2( RationalComplexNumber firstNumber,
			RationalComplexNumber secondNumber, RationalComplexNumber thirdNumber,  
			RationalComplexNumber fourthNumber) {
		matrix[0][0] = firstNumber;
		matrix[0][1] = secondNumber;
		matrix[1][0] = thirdNumber;
		matrix[1][1] = fourthNumber;
	}

	public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 secondMatrix) {
		RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][j].add(secondMatrix.matrix[i][j]);
			}
		}
		return result;
	}

	public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 secondMatrix) {
		RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
		RationalComplexNumber firstTemp = new RationalComplexNumber();
		RationalComplexNumber secondTemp = new RationalComplexNumber();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				firstTemp = this.matrix[i][0].mult(secondMatrix.matrix[0][j]);
				secondTemp = this.matrix[i][1].mult(secondMatrix.matrix[1][j]);
				result.matrix[i][j] = firstTemp.add(secondTemp);
			}
		}
		return result;
	}

	public RationalComplexVector2D multVector(
				RationalComplexVector2D rationalComplexVector) {
		// RationalComplexNumber firstTemp = new RationalComplexNumber();
		// RationalComplexNumber secondTemp = new RationalComplexNumber();
		// RationalComplexNumber thirdTemp = new RationalComplexNumber();
		// firstTemp = this.matrix[0][0].mult(rationalComplexVector.getX());
		// secondTemp = this.matrix[0][1].mult(rationalComplexVector.getY());
		// firstTemp = firstTemp.add(secondTemp);
		// secondTemp = this.matrix[1][0].mult(rationalComplexVector.getX());
		// thirdTemp = this.matrix[1][1].mult(rationalComplexVector.getY());
		// secondTemp = secondTemp.add(thirdTemp);

		return new RationalComplexVector2D (
				(this.matrix[0][0].mult(rationalComplexVector.getX())).add(this.matrix[0][1].mult(rationalComplexVector.getY())),
					(this.matrix[1][0].mult(rationalComplexVector.getX())).add(this.matrix[1][1].mult(rationalComplexVector.getY())));

				//firstTemp, secondTemp);
	}

	public RationalComplexNumber det() {
		RationalComplexNumber result = 
				this.matrix[0][0].mult(this.matrix[1][1]); 				// Task with *
		return result.sub(this.matrix[0][1].mult(this.matrix[1][0]));
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				str += this.matrix[i][j] + "   ";
			}
			str += "\n";
		}
		return str;
	}
}