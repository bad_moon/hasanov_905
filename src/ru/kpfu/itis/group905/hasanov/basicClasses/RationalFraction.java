/*
* @author Anvar Hasanov
* 11-905
* Task #1
*/

package ru.kpfu.itis.group905.hasanov.basicClasses;

public class RationalFraction {
	private int numerator;
	private int denominator;

	public RationalFraction() {
		this(0, 1);
	}

	public RationalFraction(int numerator, int denominator) {
		if (denominator == 0) {
			throw new ArithmeticException("/ by zero");
			// Собственное исключение Throw, при знаменателе = 0
		}			
		else if (denominator < 0) {
			this.denominator = denominator * (-1);
			this.numerator = numerator * (-1);
		}
		else {
			this.denominator = denominator;
			this.numerator = numerator;
		}


	}

	public void reduce() {
		int divisor = nod(this.numerator, this.denominator);
		this.numerator /= divisor;
		this.denominator /= divisor;
	}

	//nod(a, b) * nok(a, b) = a * b
	public RationalFraction add(RationalFraction secondRational) {
		this.reduce();
		secondRational.reduce();
		int nok = (this.denominator * secondRational.denominator)
				/ nod(this.denominator, secondRational.denominator);
		RationalFraction result = new RationalFraction((this.numerator 
		        * (nok / this.denominator)) 
				+ (secondRational.numerator * (nok / secondRational.denominator)), nok);
		result.reduce();
		return result;
	}

	public void add2(RationalFraction secondRational) {
		this.reduce();
		secondRational.reduce();
		int nok = (this.denominator * secondRational.denominator) 
				/ nod(this.denominator, secondRational.denominator);
		this.numerator = (this.numerator * (nok / this.denominator)) 
				+ (secondRational.numerator * (nok / secondRational.denominator));
		this.denominator = nok;
		this.reduce();

	}

	public RationalFraction sub(RationalFraction secondRational) {
		this.reduce();
		secondRational.reduce();
		int nok = (this.denominator * secondRational.denominator)
				/ nod(this.denominator, secondRational.denominator);
		RationalFraction result = new RationalFraction((this.numerator 
		        * (nok / this.denominator)) 
				- (secondRational.numerator * (nok / secondRational.denominator)), nok);
		result.reduce();
		return result;
	}

	public void sub2(RationalFraction secondRational) {
		this.reduce();
		secondRational.reduce();
		int nok = (this.denominator * secondRational.denominator) 
				/ nod(this.denominator, secondRational.denominator);
		this.numerator = (this.numerator * (nok / this.denominator)) 
				- (secondRational.numerator * (nok / secondRational.denominator));
		this.denominator = nok;
		this.reduce();

	}


	public RationalFraction mult(RationalFraction secondRational) {
		RationalFraction result = new RationalFraction(this.numerator 
				* secondRational.numerator, this.denominator * secondRational.denominator);
		result.reduce();
		return result;
	}

	public void mult2(RationalFraction secondRational) {
		this.numerator *= secondRational.numerator;
		this.denominator *= secondRational.denominator;
		this.reduce();
	}


	public RationalFraction div(RationalFraction secondRational) {
		RationalFraction result = new RationalFraction(this.numerator 
				* secondRational.denominator, this.denominator * secondRational.numerator);
		result.reduce();
		return result;
	}

	public void div2(RationalFraction secondRational) {
		if (secondRational.numerator == 0) {
			throw new ArithmeticException("/ by zero");
		}
		this.numerator *= secondRational.denominator;
		this.denominator *= secondRational.numerator;
		this.reduce();
	}

	public String toString() {
		if (this.numerator != 0) {
			if (this.numerator == this.denominator) {
				return "1";
			}
			return this.numerator + "/" + this.denominator;
		}
		return "0";

	}

	public double value() {
		return (double) this.numerator / this.denominator;
	}

	public boolean equals(RationalFraction secondRational) {
		this.reduce();
		secondRational.reduce();
		if (this.numerator == secondRational.numerator 
			&& this.denominator == secondRational.denominator) {
			return true;
		}
		return false;
	}

	public int numberPart() {
		return this.numerator / this.denominator;
	}

	private int nod(int firstNumber, int secondNumber) {
		firstNumber = Math.abs(firstNumber);
		secondNumber = Math.abs(secondNumber);
		int num = 0;
		while (secondNumber > 0) {
			num = firstNumber % secondNumber;
			firstNumber = secondNumber;
			secondNumber = num;
		}
		return firstNumber;
	}

	public int getNumerator() {
		return this.numerator;
	}

	public int getDenominator() {
		return this.denominator;
	}

	public void setNumerator(int number) {
		this.numerator = number;
	}

	public void setDenominator(int number) {
		this.denominator = number;
	}
}