/*
* @author Anvar Hasanov
* 11-905
* Task #3
*/

package ru.kpfu.itis.group905.hasanov.basicClasses;

public class Matrix2x2 {
	private double[][] matrix = new double[2][2];

	public Matrix2x2() {
		this(0);
	}

	public Matrix2x2(double number) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				matrix[i][j] = number;
			}
		}
	}

	public Matrix2x2(double[][] matrix) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				this.matrix[i][j] = matrix[i][j];
			}
		}
	}

	public Matrix2x2(double firstNumber, double secondNumber, 
			double thirdNumber, double fourthNumber) {
		matrix[0][0] = firstNumber;
		matrix[0][1] = secondNumber;
		matrix[1][0] = thirdNumber;
		matrix[1][1] = fourthNumber;
	}

	public Matrix2x2 add(Matrix2x2 secondMatrix) {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][j] + secondMatrix.matrix[i][j];
			}
		}
		return result;
	}

	public void add2(Matrix2x2 secondMatrix) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				this.matrix[i][j] += secondMatrix.matrix[i][j];
			}
		}
	}

	public Matrix2x2 sub(Matrix2x2 secondMatrix) {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][j] - secondMatrix.matrix[i][j];
			}
		}
		return result;
	}

	public void sub2(Matrix2x2 secondMatrix) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				this.matrix[i][j] -= secondMatrix.matrix[i][j];
			}
		}
	}

	public Matrix2x2 mult(Matrix2x2 secondMatrix) {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][0] * secondMatrix.matrix[0][j] 
						+ this.matrix[i][1] * secondMatrix.matrix[1][j];
			}
		}
		return result;
	}

	public void mult2(Matrix2x2 secondMatrix) {
		Matrix2x2 temp = new Matrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				temp.matrix[i][j] = this.matrix[i][0] * secondMatrix.matrix[0][j] 
						+ this.matrix[i][1] * secondMatrix.matrix[1][j];
			}
		}
	for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				this.matrix[i][j] = temp.matrix[i][j];
			}
		}
	}

	public Matrix2x2 multNumber(double number) {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][j] * number;
			}
		}
		return result;
	}

	public void multNumber2(double number) {
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				this.matrix[i][j] *= number;
			}
		}
	}


	public double det() {
		return this.matrix[0][0] * this.matrix[1][1] 
				- this.matrix[0][1] * this.matrix[1][0];
	} 

	public void transpon() {
		double temp = this.matrix[0][1];
		this.matrix[0][1] = this.matrix[1][0];
		this.matrix[1][0] = temp;

	}

	public Matrix2x2 inverseMatrix() {
		if (this.det() == 0) {
			System.out.println("Error");
			return new Matrix2x2();
		}
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][j];
			}
		}
		double temp = result.matrix[0][0];
		result.matrix[0][0] = result.matrix[1][1];
		result.matrix[1][1] = temp;
		result.matrix[0][1] *= (-1);
		result.matrix[1][0] *= (-1);

		result = result.multNumber(1.0 / this.det());
		return result;
	}

	public Matrix2x2 equivalentDiagonal() {
		Matrix2x2 result = new Matrix2x2();
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				result.matrix[i][j] = this.matrix[i][j];
			}
		}
		if (result.matrix[0][0] == 0) {
			result.matrix[0][1] = 0;
			result.matrix[1][0] = 0;

		}
		result.matrix[1][1] = (result.matrix[1][1] * result.matrix[0][0] 
				- result.matrix[0][1] * result.matrix[1][0]) / result.matrix[0][0];
		result.matrix[0][0] *= result.matrix[1][1];
		result.matrix[0][1] = 0;
		result.matrix[1][0] = 0;
		return result;
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				str += this.matrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}

	public Vector2D multVector(Vector2D vector) {
		return new Vector2D(this.matrix[0][0] * vector.getX() 
				+ this.matrix[0][1] * vector.getY(),
				this.matrix[1][0] * vector.getX() 
				+ this.matrix[1][1] * vector.getY());
	}
}