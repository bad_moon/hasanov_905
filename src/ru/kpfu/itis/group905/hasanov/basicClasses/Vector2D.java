/*
* @author Anvar Hasanov
* 11-905
* 
*/

package ru.kpfu.itis.group905.hasanov.basicClasses;

public class Vector2D {
	private double x;
	private double y;

	public Vector2D() {
		this.x = 0.0;
		this.y = 0.0;
	}

	public Vector2D(double a, double b) {
		this.x = a;
		this.y = b;
	}

	public Vector2D add(Vector2D secondVector) {
		Vector2D amound = new Vector2D(this.x + secondVector.x, this.y + secondVector.y);
		return amound;
	}

	public void add2(Vector2D secondVector) {
		this.x += secondVector.x;
		this.y += secondVector.y;
	}

	public Vector2D sub(Vector2D secondVector) {
		Vector2D difference = new Vector2D(this.x - secondVector.x,
				this.y - secondVector.y);
		return difference;
	}

	public void sub2(Vector2D secondVector) {
		this.x -= secondVector.x;
		this.y -= secondVector.y;
	}

	public Vector2D mult(double number) {
		Vector2D result = new Vector2D(this.x * number, this.y * number);
		return result;
	}

	public void mult2(double number) {
		this.x *= number;
		this.y *= number;
	}

	public String toString() {
		return "[" + x + ", " + y + "]";
	}

	public double length() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	public double scalarProduct(Vector2D secondVector) {
		return this.x * secondVector.x + this.y * secondVector.y;

	}

	public double cos(Vector2D secondVector) {
		return ((this.x * secondVector.x) + (this.y * secondVector.y)) 
				/ (Math.sqrt(this.x * this.x + this.y * this.y) 
				* Math.sqrt(secondVector.x * secondVector.x 
					+ secondVector.y * secondVector.y));
	}

	public boolean equals(Vector2D secondVector) {
		if (this.x == secondVector.x && this.y == secondVector.y) {
			return true;
		}
		return false;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}
}