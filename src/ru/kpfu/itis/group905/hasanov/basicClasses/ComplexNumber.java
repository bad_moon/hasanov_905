/*
* @author Anvar Hasanov
* 11-905
* Task #2
*/

package ru.kpfu.itis.group905.hasanov.basicClasses;

public class ComplexNumber {
	private double realNumber;
	private double imaginaryNumber;

	public ComplexNumber() {
		this(0.0, 0.0);
	}

	public ComplexNumber(double realNumber, double imaginaryNumber) {
		this.realNumber = realNumber;
		this.imaginaryNumber = imaginaryNumber;
	}

	public ComplexNumber add(ComplexNumber secondNumber) {
		return new ComplexNumber(this.realNumber + secondNumber.realNumber,
				this.imaginaryNumber + secondNumber.imaginaryNumber);
	}

	public void add2(ComplexNumber secondNumber) {
		this.realNumber += secondNumber.realNumber;
		this.imaginaryNumber += secondNumber.imaginaryNumber;
	}

	public ComplexNumber sub(ComplexNumber secondNumber) {
		return new ComplexNumber(this.realNumber - secondNumber.realNumber,
				this.imaginaryNumber - secondNumber.imaginaryNumber);
	}

	public void sub2(ComplexNumber secondNumber) {
		this.realNumber -= secondNumber.realNumber;
		this.imaginaryNumber -= secondNumber.imaginaryNumber;
	}

	public ComplexNumber multNumber(double number) {
		return new ComplexNumber(this.realNumber * number,
				this.imaginaryNumber * number);
	}

	public void multNumber2(double number) {
		this.realNumber *= number;
		this.imaginaryNumber *= number;
	}

	public ComplexNumber mult(ComplexNumber secondNumber) {
		return new ComplexNumber(this.realNumber * secondNumber.realNumber
				+ this.imaginaryNumber * secondNumber.imaginaryNumber * (-1.0),
				this.imaginaryNumber * secondNumber.realNumber
				+ this.realNumber * secondNumber.imaginaryNumber);
	}

	public void mult2(ComplexNumber secondNumber) {
		double copyRealNumber = this.realNumber;
		this.realNumber = this.realNumber * secondNumber.realNumber 
				+ this.imaginaryNumber * secondNumber.imaginaryNumber * (-1.0);
		this.imaginaryNumber = this.imaginaryNumber * secondNumber.realNumber
				+ copyRealNumber * secondNumber.imaginaryNumber;
	}

	public ComplexNumber div(ComplexNumber secondNumber) {
		double newRealNumber = this.realNumber * secondNumber.realNumber
				+ this.imaginaryNumber * secondNumber.imaginaryNumber;
		double newImaginaryNumber = this.realNumber * secondNumber.imaginaryNumber 
				* (-1.0) + this.imaginaryNumber * secondNumber.realNumber;
		double denominator = secondNumber.realNumber * secondNumber.realNumber 
				+ secondNumber.imaginaryNumber * secondNumber.imaginaryNumber;
		return new ComplexNumber(newRealNumber / denominator,
				newImaginaryNumber / denominator);
	}

	public void div2(ComplexNumber secondNumber) {
		double denominator = secondNumber.realNumber * secondNumber.realNumber 
				+ secondNumber.imaginaryNumber * secondNumber.imaginaryNumber;
		double copyRealNumber = this.realNumber;
		this.realNumber = (this.realNumber * secondNumber.realNumber
				+ this.imaginaryNumber * secondNumber.imaginaryNumber) 
				/ denominator;
		this.imaginaryNumber = (copyRealNumber * secondNumber.imaginaryNumber 
			* (-1.0) + this.imaginaryNumber * secondNumber.realNumber) / denominator;
	}

	public double length() {
		return Math.sqrt(this.realNumber * this.realNumber 
				+ this.imaginaryNumber * this.imaginaryNumber);
	}

	public String toString() {
		if (realNumber == 0) {
			if (imaginaryNumber == 0) {
				return "0";
			}
			if (imaginaryNumber % 1 == 0) {
				return ((int) imaginaryNumber) + "i";
			}
			return imaginaryNumber + "i";
		}
		if (imaginaryNumber == 0) {
			if (realNumber % 1 == 0) {
				return ((int) realNumber) + "";
			}
			return realNumber + "";
		}
		if (imaginaryNumber < 0) {
			if (realNumber % 1 == 0 && imaginaryNumber % 1 == 0) {
				return ((int) realNumber) + " - " + (-1) * ((int) imaginaryNumber) + "i";
			}
			if (realNumber % 1 == 0) {
				return ((int) realNumber) + " - " + (-1.0) * imaginaryNumber + "i";
			}
			if (imaginaryNumber % 1 == 0) {
				return realNumber + " - " + (-1) * ((int) imaginaryNumber) + "i";
			}
			return realNumber + " - " + (-1.0) * imaginaryNumber + "i";
		}
		if (realNumber % 1 == 0 && imaginaryNumber % 1 == 0) {
				return ((int) realNumber) + " + " + ((int) imaginaryNumber) + "i";
			}
			if (realNumber % 1 == 0) {
				return ((int) realNumber) + " + " + imaginaryNumber + "i";
			}
			if (imaginaryNumber % 1 == 0) {
				return realNumber + " + " + ((int) imaginaryNumber) + "i";
			}
		return realNumber + " + " + imaginaryNumber + "i";
	}

	public double arg() {
		double radian = Math.atan(this.imaginaryNumber / this.realNumber);
		if (radian != 0) {
			return Math.PI + radian;
		}
		return radian;
	}

	public void pow(double power) {
		double lengthInPower = Math.pow(this.length(), power);
		this.realNumber = Math.cos(this.arg() * power) * lengthInPower;
		this.imaginaryNumber = Math.sin(this.arg() * power) * lengthInPower;
	}

	public boolean equals(ComplexNumber secondNumber) {
		double eps = 0.000001;
		if (Math.abs(this.realNumber - secondNumber.realNumber) < eps
				&& Math.abs(this.imaginaryNumber 
					- secondNumber.imaginaryNumber) < eps) {
			return true;
		}
		return false;
	}
}