/**
* @author Anvar Hasanov
* 11-905
* Task #2
*/

package ru.kpfu.itis.group905.hasanov.halloween;

public class Pumpkin {
	private Skeleton skeleton;
	private int town;

	public Pumpkin() {
		this(0, new Skeleton());
	}

	public Pumpkin(int town, Skeleton skeleton) {
		this.skeleton = skeleton;
		this.town = town;
	}

	public String toString() {
		return skeleton.getName() + " " + town;
	}
}