/**
* @author Anvar Hasanov
* 11-905
* Task #2
*/

package ru.kpfu.itis.group905.hasanov.halloween;

public class Halloween {
	public static void main(String[] args) {
		int halloweenTown = 10;
		Skeleton bones = new Skeleton("bones");
		Pumpkin king = new Pumpkin(halloweenTown, bones);

		Skeleton skellington = bones;
		skellington.setName("skellington");
		halloweenTown = 5;
		System.out.println(king);
	}
}