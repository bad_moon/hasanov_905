/**
* @author Anvar Hasanov
* 11-905
* Task #2
*/

package ru.kpfu.itis.group905.hasanov.halloween;

public class Skeleton {
	private String name;

	public Skeleton() {
		this("no name");
	}

	public Skeleton(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
}