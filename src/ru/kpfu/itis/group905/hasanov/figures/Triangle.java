/**
* @author Anvar Hasanov
* 11-905
* Task #1 HW_20
*/

package ru.kpfu.itis.group905.hasanov.figures;

public class Triangle extends Figure {
	private final String name = "Triangle";
	private double sideA;
	private double sideB;
	private double sideC;

	public Triangle() {
		this(0.0);
	}

	public Triangle(double side) {
		this(side, side, side);
	}

	public Triangle(double sideA, double sideB, double sideC) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.sideC = sideC;
	}

	public double square() {
		return this.sideA * this.sideB / 2.0;
	}

	public double perimeter() {
		return this.sideA + this.sideB + this.sideC;
	}

	public String getName() {
		return this.name;
	}

	public String print() {
		return this.getName();
	}
}