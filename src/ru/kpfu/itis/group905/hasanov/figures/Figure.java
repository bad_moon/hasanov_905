/**
* @author Anvar Hasanov
* 11-905
* Task #1 HW_20
*/

package ru.kpfu.itis.group905.hasanov.figures;

public abstract class Figure implements PrintableFigure {
	public abstract double square();

	public abstract double perimeter();

	public abstract String getName();
	
	public String info() {
		return "\nSquare = " + this.square() + "\nPerimetr = " + this.perimeter();
	}
}