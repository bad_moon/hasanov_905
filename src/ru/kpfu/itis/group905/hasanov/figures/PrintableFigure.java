/**
* @author Anvar Hasanov
* 11-905
* Task #2 HW_20
*/

package ru.kpfu.itis.group905.hasanov.figures;

public interface PrintableFigure {
	public String print();
}