/**
* @author Anvar Hasanov
* 11-905
* Task #1 HW_20
*/

package ru.kpfu.itis.group905.hasanov.figures;

public class Circle extends Figure {
	public final String name = "Circle";
	private double radius;

	public Circle() {
		this(0.0);
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public double square() {
		return radius * radius * Math.PI;
	}

	public double perimeter() {
		return 2.0 * radius * Math.PI;
	}

	public String getName() {
		return this.name;
	}

	public String print() {
		return this.getName();
	}
}