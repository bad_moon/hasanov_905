/**
* @author Anvar Hasanov
* 11-905
* Task #1 HW_20
*/

package ru.kpfu.itis.group905.hasanov.figures;

public class Main {
	public static void main(String[] args) {
		Figure[] figures = new Figure[] {new Circle(2.0),
				new Triangle(4.0, 3.0, 5.0), new Rectangle(4.0, 2.0)};

		for (Figure newFigure : figures) {
			System.out.print(newFigure.print());
			System.out.println(newFigure.info() + "\n");
		}
	}
}