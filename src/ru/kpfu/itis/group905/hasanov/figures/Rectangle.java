/**
* @author Anvar Hasanov
* 11-905
* Task #1 HW_20
*/

package ru.kpfu.itis.group905.hasanov.figures;

public class Rectangle extends Figure {
	private final String name = "Rectangle";
	private double sideA;
	private double sideB;

	public Rectangle() {
		this(0.0);
	}

	public Rectangle(double side) {
		this(side, side);
	}

	public Rectangle(double sideA, double sideB) {
		this.sideA = sideA;
		this.sideB = sideB;
	}

	public double square() {
		return sideA * sideB;
	} 

	public double perimeter() {
		return 2.0 * (sideA + sideB);
	}

	public String getName() {
		return this.name;
	}

	public String print() {
		return this.getName();
	}
}