package ru.kpfu.itis.group905.hasanov.kontrol;

// Ingredient is enum because a limited set of valid values must be used
public enum Ingredient {
	HAM ("ham"),
	EGG ("egg"),
	JELLY ("jelly"),
	BUTTER ("butter"),
	BREAD ("bread"),
	SALAD ("salad"),
	TOMATO ("tomato"),
	POTATO ("potato"),
	CHEESE ("cheese"),
	SOUSE ("souse"),
	CUCUMBER ("cucumber");

	private String ingredient;

	Ingredient(String ingredient) {
		this.ingredient = ingredient;
	}

	public String getIngredient() {
		return this.ingredient;
	}

	public String toString() {
		return this.ingredient;
	}
	
}