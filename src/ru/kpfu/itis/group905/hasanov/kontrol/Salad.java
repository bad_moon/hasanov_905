package ru.kpfu.itis.group905.hasanov.kontrol;

import java.io.*;
import java.util.Scanner;

public class Salad implements Cookable {
	private String name = "salad";
	private String ingredients;

	public Salad() {
		this("nothing");
	}

	public Salad(String ingredients) {
		this.ingredients = ingredients;
	}

	public void prepare() {
		Scanner sc = new Scanner(System.in);

		Ingredient[] ingArr = Ingredient.values();

		System.out.println("Enter the ingredients one at a time:");
		String ingredient = sc.nextLine();
		boolean flag = false;
		while (!ingredient.equals("stop")) {
			if  (ingredient.equals("butter") || ingredient.equals("jelly")) {
				System.out.println("The " + ingredient + "in the salad tastes terrible.");
			}
			else {
				for (int i = 0; i < ingArr.length; i++) {
					if (ingArr[i].getIngredient().equals(ingredient)) {
						if (this.ingredients.equals("nothing")) {
							this.ingredients = ingredient;
							System.out.println("Ingredient accepted");
							flag = true;
							break;
						}
						else {
							this.ingredients += "," + ingredient;
							System.out.println("Ingredient accepted");
							flag = true;
							break;
						}
					}
				}

				if (!flag) {
					System.out.println("There is no such ingredient");
				}

				flag = false;
			}

			System.out.println("Enter next ingredient");
			ingredient = sc.nextLine();
		}
	}

	public void prepareInFile(String fileName) throws IOException {
		String path = "C:/Users/Badmoon/Desktop/Git/hasanov_905/src/ru/kpfu/itis/group905/hasanov/kontrol";
		BufferedReader line = new BufferedReader(new FileReader(path + fileName));
		this.ingredients = line.readLine();		
	}

	public Dish cook() {
		return new Dish(this.name, this.ingredients);
	}
}