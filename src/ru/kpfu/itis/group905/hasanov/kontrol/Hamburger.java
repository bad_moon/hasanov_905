package ru.kpfu.itis.group905.hasanov.kontrol;

import java.util.Scanner;

public class Hamburger implements Cookable {
	private String name = "hamburger";
	private String type;
	private String ingredients;

	public Hamburger() {
		this.type = "null";
		this.ingredients = "null";
	}

	public void prepare() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Which hamburger do you want?");
		String input = sc.nextLine();

		while (true) {
			if (!input.equals("jelly-butter sandwich") && !input.equals("ham")) {
				System.out.println("This type of food is not available, check back later");
				System.out.println("Invest something else");
				input = sc.nextLine();
				continue;
			}
			else {
				this.type = input;
				System.out.println("Accepted!");
				switch (input) {
					case "jelly-butter sandwich":
						this.ingredients = "bread,butter,jelly,bread";
						break;
					case "ham":
						this.ingredients = "bread,ham,souse,cucumber,cheese,bread";
				}
				break;
			}
		}

	}


	public Dish cook() {
		return new Dish(this.name, this.ingredients);
	} 
}