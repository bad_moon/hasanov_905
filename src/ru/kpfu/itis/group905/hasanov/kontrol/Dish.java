package ru.kpfu.itis.group905.hasanov.kontrol;

public class Dish {
	private String name;
	private String status;
	private String[] ingredients;

	public Dish(String name, String ingredients) {
		this.name = name;
		this.ingredients = ingredients.split(",");
		this.status = "cooked";
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	public String toString() {
		String temp = ingredients[0];
		for (int i = 1; i < ingredients.length; i++) {
			temp += ", " + ingredients[i];
		}
		
		return this.name + " with " + temp;
	}
}
