/**
* @author Anvar Hasanov
* 11-905
* kontrol work
*/

/*
* Task number one completed,
* I started to do the second, so the main was changed, did the part
*/

package ru.kpfu.itis.group905.hasanov.kontrol;

import java.util.Scanner;

public class Main {
    static final int COUNT_ORDERS = 10;
    static int realCount = 0;
    static Dish[] orders = new Dish[COUNT_ORDERS];
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String command = "";
        int random = 0;

        for (int itr = 0; itr < COUNT_ORDERS; itr++) {
            System.out.println("What are you want?");
            command = scanner.nextLine();
            if (command.equals("stop")) {
                break;
            }
            start_kitchen(command);

            for (int j = 0; j < realCount; j++) {
                random = (int) (Math.random() * 10);
                if (random >= 6 && orders[j].getStatus().equals("cooked")) {
                    orders[j].setStatus("taken");
                }
            }

            random = (int) (Math.random() * 100);
            if (random > 60) {
                break;
            }
        }

        for (int i = 0; i < realCount; i++) {
            System.out.println(orders[i] + ". Status: " 
                    + orders[i].getStatus());
        }
    }

    public static void start_kitchen(String command) {
        switch (command) {
            case "hamburger":
                orders[realCount] = make_food(new Hamburger());
                realCount++;
                break;
            case "salad":
                orders[realCount] = make_food(new Salad());
                realCount++;
                break;
            default:
                System.out.println("Sorry, for now we have only salads and hamburgers. Come later");
                break; //
        }
    }

    public static Dish make_food(Cookable eat) {
        eat.prepare();

        // Dish dish = eat.cook();

        return eat.cook();

        //System.out.println("Your " + dish + " is ready");

    }
}