package ru.kpfu.itis.group905.hasanov.kontrol;

// Cookable - interface, because it has no implementation, it is needed only for inheritance
public interface Cookable { 
	public void prepare();

	public Dish cook();
	
}
