package ru.kpfu.itis.group905.hasanov;
 
import java.util.Scanner;
import ru.kpfu.itis.group905.hasanov.basicClasses.*;
import ru.kpfu.itis.group905.hasanov.complexClasses.*;
import ru.kpfu.itis.group905.hasanov.money.Money;

public class Main {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		RationalFraction rational1 = new RationalFraction(4, 5);
		RationalFraction rational2 = new RationalFraction(3, 9);
		RationalFraction rationalAdd2 = new RationalFraction(1, 8);
		RationalFraction rationalSub2 = new RationalFraction(6, 7);
		RationalFraction rationalMult2 = new RationalFraction(9, 2);
		RationalFraction rationalDiv2 = new RationalFraction(3, 9);
		RationalFraction rational1copy = new RationalFraction(4, 5);
	
		System.out.println(rational2.add(rational1));
		rationalAdd2.add2(rational1);
		System.out.println(rationalAdd2);

		System.out.println(rational2.sub(rational1));
		rationalSub2.sub2(rational1);
		System.out.println(rationalSub2);

		System.out.println(rational2.mult(rational1));
		rationalMult2.mult2(rational1);
		System.out.println(rationalMult2);

		System.out.println(rational2.div(rational1));
		rationalDiv2.div2(rational1);
		System.out.println(rationalDiv2);

		System.out.println(rational1.value());

		System.out.println(rational1.equals(rational1copy));

		System.out.println(rational1.numberPart());





		Matrix2x2 matrix1 = new Matrix2x2();
		System.out.println(matrix1);

		Matrix2x2 matrix2 = new Matrix2x2(5);
		System.out.println(matrix2);

		double[][] arr = new double[][] {{1, 2}, {3, 4}};
		Matrix2x2 matrix3 = new Matrix2x2(arr);
		System.out.println(matrix3);

		Matrix2x2 matrix4 = new Matrix2x2(1, 2, 3, 4);
		System.out.println(matrix4);

		Matrix2x2 matrix5 = new Matrix2x2(3, 6, 1, 9);
		Matrix2x2 matrix6 = new Matrix2x2(0, 1, 3, 5);
		Matrix2x2 matrixAdd2 = new Matrix2x2(0, 1, 3, 5);
		Matrix2x2 matrixSub2 = new Matrix2x2(0, 1, 3, 5);
		Matrix2x2 matrixMultNumber2 = new Matrix2x2(0, 1, 3, 5);
		Matrix2x2 matrixMult2 = new Matrix2x2(0, 1, 3, 5);
		Matrix2x2 matrixTran = new Matrix2x2(3, 6, 1, 9);

		System.out.println(matrix6.add(matrix5));
		matrixAdd2.add2(matrix5);
		System.out.println(matrixAdd2);

		System.out.println(matrix6.sub(matrix5));
		matrixSub2.sub2(matrix5);
		System.out.println(matrixSub2);

		System.out.println(matrix6.multNumber(3));
		matrixMultNumber2.multNumber2(3);
		System.out.println(matrixMultNumber2);

		System.out.println(matrix6.mult(matrix5));
		matrixMult2.mult2(matrix5);
		System.out.println(matrixMult2);

		System.out.println(matrix5.det() + "\n");

		matrixTran.transpon();
		System.out.println(matrixTran);

		System.out.println(matrix5.inverseMatrix());
		System.out.println("Это не ошибка! =)" + "\n");

		System.out.println(matrix5.equivalentDiagonal());

		Vector2D vector = new Vector2D(2, 3);
		System.out.println(matrix5.multVector(vector));

		


		ComplexNumber complex = new ComplexNumber(1, 2);
		ComplexNumber complex1 = new ComplexNumber(2, 3);
		ComplexNumber complexAdd2 = new ComplexNumber(1, 2);
		ComplexNumber complexSub2 = new ComplexNumber(1, 2);
		ComplexNumber complexMultNumber2 = new ComplexNumber(1, 2);
		ComplexNumber complexMult2 = new ComplexNumber(1, 2);
		ComplexNumber complexDiv2 = new ComplexNumber(1, 2);
		ComplexNumber complexPow = new ComplexNumber(1, 2);
		ComplexNumber complexEquals = new ComplexNumber(1, 2);

		System.out.println(complex.add(complex1));
		complexAdd2.add2(complex1);
		System.out.println(complexAdd2);

		System.out.println(complex.sub(complex1));
		complexSub2.sub2(complex1);
		System.out.println(complexSub2);

		System.out.println(complex.multNumber(5));
		complexMultNumber2.multNumber2(5);
		System.out.println(complexMultNumber2);

		System.out.println(complex.mult(complex1));
		complexMult2.mult2(complex1);
		System.out.println(complexMult2);

		System.out.println(complex.div(complex1));
		complexDiv2.div2(complex1);
		System.out.println(complexDiv2);

		System.out.println(complex.arg());

		complexPow.pow(5);
		System.out.println(complexPow);

		System.out.println(complex.equals(complexEquals));
		///////////////////////////
		///////////////////////////
		///////////////////////////

		System.out.println("//////////////////////");
		System.out.println("Next Task");
		System.out.println("//////////////////////");

		/////////////////////////////
		///////////////////////////
		///////////////////////////

		RationalVector2D vec = new RationalVector2D(
				new RationalFraction(1, 2), new RationalFraction(1, 3));
		RationalVector2D vec1 = new RationalVector2D(
				new RationalFraction(1, 3), new RationalFraction(1, 5));

		System.out.println(vec.add(vec1));
		System.out.println(vec.length());
		System.out.println(vec.scalarProduct(vec1));
		System.out.println(vec.equals(vec1) + "\n -------------");

//////////////////

		ComplexVector2D vecCom = new ComplexVector2D(
				new ComplexNumber(3, 4), new ComplexNumber(2, 1));
		ComplexVector2D vecCom1 = new ComplexVector2D(
				new ComplexNumber(1, 2), new ComplexNumber(4, 2));

		System.out.println(vecCom.add(vecCom1));
		System.out.println(vecCom.equals(vecCom1));
		System.out.println(vecCom.scalarProduct(vecCom1)); 			   // Task with *
		System.out.println(" -------------");

/////////////////

		RationalMatrix2x2 ratMat1 = new RationalMatrix2x2(
				new RationalFraction(1, 2));
		RationalMatrix2x2 ratMat2 = new RationalMatrix2x2(new RationalFraction(1, 3),
				new RationalFraction(2, 3), new RationalFraction(6, 4),
				new RationalFraction(6, 4));

		System.out.println(ratMat1.add(ratMat2));
		System.out.println(ratMat1.mult(ratMat2));
		System.out.println(ratMat2.det() + "\n");
		System.out.println(ratMat1.multVector(vec) + "\n -------------");

/////////////////

		ComplexMatrix2x2 comMat1 = new ComplexMatrix2x2(new ComplexNumber(2, 3));
		ComplexMatrix2x2 comMat2 = new ComplexMatrix2x2(new ComplexNumber(7, 2),
				new ComplexNumber(1, 1), new ComplexNumber(5, 2),
				new ComplexNumber(2, 4));

		System.out.println(comMat1.add(comMat2));
		System.out.println(comMat1.mult(comMat2));
		System.out.println(comMat2.det() + "\n");  					 // Task with *
		System.out.println(comMat2.multVector(vecCom) + "\n -------------");

/////////////////

		RationalComplexNumber ratComNum1 = new RationalComplexNumber(
				new RationalFraction(1, 3), new RationalFraction(2, 3));
		RationalComplexNumber ratComNum2 = new RationalComplexNumber(
				new RationalFraction(4, 3), new RationalFraction(3, 2));

		System.out.println(ratComNum1.add(ratComNum2));
		System.out.println(ratComNum1.sub(ratComNum2));
		System.out.println(ratComNum1.mult(ratComNum2) + "\n -------------");

/////////////////

		RationalComplexNumber ratComNum3 = new RationalComplexNumber(
				new RationalFraction(2, 3), new RationalFraction(3, 3));
		RationalComplexNumber ratComNum4 = new RationalComplexNumber(
				new RationalFraction(7, 3), new RationalFraction(3, 5));
		RationalComplexVector2D ratComVec1 = 
				new RationalComplexVector2D(ratComNum1, ratComNum2);
		RationalComplexVector2D ratComVec2 = 
				new RationalComplexVector2D(ratComNum3, ratComNum4);

		System.out.println(ratComVec1.scalarProduct(ratComVec2));   // Task with *
		System.out.println(ratComVec1.add(ratComVec2) + "\n -------------");

/////////////////

		RationalComplexMatrix2x2 ratComMat1 = new RationalComplexMatrix2x2(
				ratComNum1);
		RationalComplexMatrix2x2 ratComMat2 = new RationalComplexMatrix2x2(
				ratComNum1, ratComNum2, ratComNum3, ratComNum4);

		System.out.println(ratComMat1.add(ratComMat2));
		System.out.println(ratComMat1.mult(ratComMat2));
		System.out.println(ratComMat1.multVector(ratComVec1));
		System.out.println(ratComMat2.det() + "\n");             // Task with *

		///////////////////////////
		///////////////////////////
		///////////////////////////

		System.out.println("//////////////////////");
		System.out.println("Next Task");
		System.out.println("//////////////////////");

		/////////////////////////////
		///////////////////////////
		///////////////////////////

		Money money = new Money(0, (short) 190);
		Money money1 = new Money(11, (short) 25);
		Money moneyAdd = new Money(0, (short) -1010);
		Money moneySub = new Money(1, (short) 48);

		System.out.println(moneyAdd.add(money1));

		System.out.println(moneySub.sub(money));

		System.out.println(money.div(money1));

		RationalFraction rational = new RationalFraction(2, 1);
		System.out.println(money.divRational(rational));

		System.out.println(money.multRational(rational));

		System.out.println(money.equals(money1));
		System.out.println(money.equals(money));
	}
}
