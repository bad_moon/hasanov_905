package ru.vsharage.subscription;

import ru.vsharage.user.User;

public class Subscription {
	private User subscriber;
	private User subscription;

	public Subscription(User subscriber, User subscription) {
		this.subscriber = subscriber;
		this.subscription = subscription;
	}

	public User getSubscriber() {
		return this.subscriber;
	}

	public User getSubscription() {
		return this.subscription;
	}

	public String toString() {
		return this.subscriber.getName() + " subscribe to " 
				+ this.subscription.getName();
	}
}