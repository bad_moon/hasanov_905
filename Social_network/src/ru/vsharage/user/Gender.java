package ru.vsharage.user;

public enum Gender {
	MAN ("man"),
	WOMEN ("women");

	private String gender;

	Gender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return this.gender;
	}

	public String toString() {
		return this.gender + "";
	}
}