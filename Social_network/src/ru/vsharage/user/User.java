/**
* @author Anvar Hasanov
* 11-905
* Task #2 1k_22p
*/

package ru.vsharage.user;

public class User {
	private int id;
	private String name;
	private Gender gender;
	private String email;
	private String password;

	public User(String[] array) {
		this.id = Integer.parseInt(array[0]);
		this.name = array[1];
		this.password = array[2];
		switch (array[3]) {
			case "man":
				this.gender = Gender.MAN;
				break;
			case "women":
				this.gender = Gender.WOMEN;
		}
		this.email = array[4];
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getGender() {
		return this.gender + "";
	}

	public String toString() {
		return "id " + this.id + "\nName: " + this.name 
				+ "\nGender: " + this.gender + "\nemail: " + this.email;
	}
}
