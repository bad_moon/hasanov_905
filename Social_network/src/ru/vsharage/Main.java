/**
* @author Anvar Hasanov
* 11-905
* Task #2 1k_22p
*/

package ru.vsharage;

import java.io.*;
import ru.vsharage.user.User;
import ru.vsharage.message.Message;
import ru.vsharage.subscription.Subscription;


public class Main {
	static final int COUNT_USERS = 10;
	static User[] users = new User[COUNT_USERS];
	static final int COUNT_MESSAGES = 12;
	static Message[] messages = new Message[COUNT_MESSAGES];
	static final int COUNT_SUBSCRIP = 10;
	static Subscription[] subscriptions = new Subscription[COUNT_SUBSCRIP];

	public static void main(String[] args) throws IOException {
		String path = "C:/Users/Badmoon/Desktop/Git/hasanov_905/Social_network/database/";
		BufferedReader usData = new BufferedReader(new FileReader(path + "users.csv"));
		BufferedReader mesData = new BufferedReader(new FileReader(path + "messages.csv"));
		BufferedReader subscData = new BufferedReader(new FileReader(path + "subscriptions.csv"));

		/// User
		String line = usData.readLine();
		line = usData.readLine();
		String[] arrayLine;

		for (int i = 0; i < COUNT_USERS; i++) {
			arrayLine = line.split(",");
			users[i] = new User(arrayLine);
			line = usData.readLine();
		}
		///

		/// Message
		line = mesData.readLine();
		line = mesData.readLine();

		for (int i = 0; i < COUNT_MESSAGES; i++) {
			arrayLine = line.split(",");
			messages[i] = new Message(users[Integer.parseInt(arrayLine[0]) - 1], 
					users[Integer.parseInt(arrayLine[1]) - 1], arrayLine[2], arrayLine[3],
					arrayLine[4]);
			line = mesData.readLine();
		}
		///

		/// Subscription
		line = subscData.readLine();
		line = subscData.readLine();

		for (int i = 0; i < COUNT_SUBSCRIP; i++) {
			arrayLine = line.split(",");
			subscriptions[i] = new Subscription(
					users[Integer.parseInt(arrayLine[0]) - 1],
					users[Integer.parseInt(arrayLine[1]) - 1]);
			line = subscData.readLine();
		}
		///

		/// First item
		System.out.println(dialog(users[1], users[8]));

		/// Second item
		System.out.println(friends(users[1]));

		/// Third item
		System.out.println(countMesGender());

		/// Fourth item
		System.out.println(statistics());

	}

	public static String dialog(User firstUser, User secondUser) {
		String result = "";
		for (int i = 0; i < COUNT_MESSAGES; i++) {
			if  (messages[i].getSender().getId() == firstUser.getId()
					&& messages[i].getReceiver().getId() == secondUser.getId()) {
				result += firstUser.getName() + ": " + messages[i].getText() 
						+ " (" + messages[i].getStatus() + ")\n";
			}

			else if (messages[i].getSender().getId() == secondUser.getId()
					&& messages[i].getReceiver().getId() == firstUser.getId()) {
				result += secondUser.getName() + ": " + messages[i].getText() 
						+ " (" + messages[i].getStatus() + ")\n";
			}
		}
		return result;
	} 

	public static String friends(User user) {
		boolean flag = false;
		String result = "";
		for (int i = 0; i < COUNT_SUBSCRIP; i++) {
			if (subscriptions[i].getSubscriber().getId() == user.getId()) {
				for (int j = 0; j < COUNT_SUBSCRIP; j++) {
					if (subscriptions[j].getSubscriber().getId() 
							== subscriptions[i].getSubscription().getId()
							&& subscriptions[j].getSubscription().getId() == user.getId()) {
						result += user.getName() + " friends with " 
								+ subscriptions[j].getSubscriber().getName() + "\n";
						flag = true;
					}
				}
			}	
		}

		if (!flag) {
			return user.getName() + " don't have friends";
		}

		return result;
	}

	public static String countMesGender() {
		int[][] temp = new int[COUNT_MESSAGES][2];
		temp[0][0] = messages[0].getSender().getId();
		temp[0][1] += 1;
		int count = 1;
		boolean flag = false;
		for (int i = 1; i < COUNT_MESSAGES; i++) {
			for (int j = 0; j < count; j++) {
				if (temp[j][0] == messages[i].getSender().getId()) {
					temp[j][1] += 1;
					flag = true;
					break;
				}
			}

			if (!flag) {
				temp[count][0] = messages[i].getSender().getId();
				temp[count][1] += 1;
				count++;
				flag = false;
			}
		}

		int max = 0;
		int userId = 0;
		for (int i = 0; i < count; i++) {
			if (temp[i][1] > max) {
				max = temp[i][1];
				userId = temp[i][0];
			}
		}

		return users[userId - 1].getGender() + " send more messages\n";
	}

	public static String statistics() {
		String gen = "";
		int[][] arrStat = new int[2][2];
		// first - gender, second - count of read messages 
		// [0] - man
		// [1] - women
		for (int i = 1; i < COUNT_MESSAGES; i++) {
			gen = messages[i].getSender().getGender();
			if (!gen.equals(messages[i].getReceiver().getGender())) {
				switch (gen) {
					case "man":
						arrStat[0][0] += 1;
						if (equalsOfRead(messages[i])) {
							arrStat[0][1] += 1;
						} 
						break;
					case "women":
						arrStat[1][0] += 1;
						if (equalsOfRead(messages[i])) {
							arrStat[1][1] += 1;							
						}
				}
			}
		}

		int statMan = (int)(1.0 * arrStat[0][1] / arrStat[0][0] * 100.0); 
		int statWom = (int)(1.0 * arrStat[1][1] / arrStat[1][0] * 100.0);
		return "Men sent " + arrStat[0][0] + " messages to women and " + statMan 
				+ "% of them are read\nWomen sent " + arrStat[1][0] + " messages to men and "
				+ statWom + "% of them are read";
	}

	public static boolean equalsOfRead(Message message) {
		if (message.getStatus().equals("read")) {
			return true;
		}
		return false;
	}
}