package ru.vsharage.message;

public enum Status {
	READ ("read"),
	UNREAD ("unread");

	private String status;

	Status(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}

	public String toString() {
		return this.status + "";
	}

}