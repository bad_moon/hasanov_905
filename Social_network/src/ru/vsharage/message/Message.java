package ru.vsharage.message;

import ru.vsharage.user.User;

public class Message {
	private User sender;
	private User receiver;
	private String timesent;
	private String text;
	private Status status;

	public Message(User sender, User receiver, String timesent, String text,
				String status) {
		this.sender = sender;
		this.receiver = receiver;
		this.timesent = timesent;
		this.text = text;
		switch (status) {
			case "read":
				this.status = Status.READ;
				break;
			case "unread":
				this.status = Status.UNREAD;
		}
	}

	public User getSender() {
		return this.sender;
	}

	public User getReceiver() {
		return this.receiver;
	}

	public String getText() {
		return this.text;
	}

	public String getStatus() {
		return this.status + "";
	}
	
	public String toString() {
		return "From " + this.sender.getName() + " to " + this.receiver.getName() 
				+ ". Time: " + this.timesent + ".\nText: " + this.text 
				+ ".\nStatus: " + this.status + "\n";
	}
}