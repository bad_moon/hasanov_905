/*
* @author Anvar Hasanov
* 11-905
* Task #1
*/

public class Player {
	private String name;
	private int health;

	Player() {
		this("No name", 10);
	}

	Player(String name, int health) {
		this.name = name;
		this.health = health;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setHp(int health) {
		this.health = health;
	}

	public int getHp() {
		return health;
	}
}