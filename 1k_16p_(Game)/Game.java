/*
* @author Anvar Hasanov
* 11-905
* Task #1
*/

import java.util.Scanner;

public class Game {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Player plr1 = new Player();
		newName(plr1, "first");

		Player plr2 = new Player();
		newName(plr2, "second");

		System.out.println("Hello " + plr1.getName() + " and " + plr2.getName());

		while (true) {
			hit(plr1, plr2);
			if (plr2.getHp() <= 0) {
				win(plr1);
				break;
			}

			hit(plr2, plr1);
			if (plr1.getHp() <= 0) {
				win(plr2);
				break;
			}
		}

	}

	public static void hit(Player firstPlayer, Player secondPlayer) {
		System.out.print("Player \"" + firstPlayer.getName() + "\" enter hit power: ");
		int hitPower = sc.nextInt();
		int rnd = 1 + (int) (Math.random() * 9);
		if (hitPower <= rnd) {
			secondPlayer.setHp(secondPlayer.getHp() - hitPower);
			System.out.println(secondPlayer.getName() + " health = " + secondPlayer.getHp());
		}
		else {
			System.out.println("Oops failed");
		}
		System.out.println();
	}

	public static void newName(Player player, String number) {
		System.out.print("Enter " + number + " player name: ");
		String name = sc.nextLine();
		player.setName(name);
	}

	public static void win(Player player) {
		System.out.println(player.getName() + " win! \nGame over");
	}
}