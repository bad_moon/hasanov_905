import java.util.Scanner;

class Task_1 {
	public static void main(String[] args) {
		Scanner n = new Scanner(System.in);
		int a = n.nextInt();
		while (a != 0) {
			System.out.println(konvert(a));
			System.out.println();

			a = n.nextInt();
		}
	}

	static String konvert(int num) {
		String res = "";
		while (num > 0) {
			res += num % 2;
			num /= 2;
		}
		res = new StringBuffer(res).reverse().toString();
		return res;
	}
}