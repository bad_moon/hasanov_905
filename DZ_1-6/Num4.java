/**
* @author Anvar Hasanov
* 11-905
* Task Number 4
*/

import java.util.Scanner;

class Num4 {
    public static void main(String[] args) {
        System.out.print("Enter ---> ");
        Scanner txt = new Scanner(System.in);
        int N = txt.nextInt(), res = 0;
        while ( N != 0) {
            res++;
            N /= 10;
        }
        System.out.println("Number of characters = " + res);
    }
}
