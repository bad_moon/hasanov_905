/**
* @author Anvar Hasanov
* 11-905
* Task Number 6
*/

class Num6 {
    public static void main(String[] args) {
        int m = Integer.parseInt(args[0]); 
		double res = 0.0;
        for (int i = 1; i <= m; i++) {
			res += ( 1.0 / i );
		}
        System.out.println("Result = " + res);   
    }
}