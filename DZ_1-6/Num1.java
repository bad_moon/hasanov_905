/**
* @author Anvar Hasanov
* 11-905
* Task Number 1
*/

import java.util.Scanner;

class Num1 {
    public static void main(String[] args) {
        System.out.println("Gap [-3;0]");
        Scanner txt = new Scanner(System.in);
        double N = txt.nextDouble();
        double res = 0;
        if (N >= -3 && N <= -2) {
            res = N * N;
            System.out.println(res);
        }
        else if (N > -2 && N <= 0) {
            res = 2 * N + 8;
            System.out.println(res);
        }
        else {
			System.out.println("The number does not match the gap!");     
		}
    }
}

