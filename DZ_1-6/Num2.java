/**
* @author Anvar Hasanov
* 11-905
* Task Number 2
*/

import java.util.Scanner;

class Num2 {
    public static void main(String[] args) {
        double res = 0;
        int N = Integer.parseInt(args[0]);
        if (N <= -1) {
			res = (double) ((-1) * (N * N) - (4 * N) - 3);
		}
        else if (N > -1 && N <= 1) {
			res = (double) N + 1;
		}
        else if (N > 1) {
			res = (double) 2 / N;
		}
        System.out.println("Result == " + res);   
    }
}
