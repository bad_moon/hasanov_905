/**
* @author Anvar Hasanov
* 11-905
* Task Number 5
*/

import java.util.Scanner;

class Num5 {
    public static void main(String[] args) {
        System.out.print("Enter ---> ");
        Scanner n = new Scanner(System.in);
        int MyNum = n.nextInt();

        System.out.print("Enter degree ---> ");
        Scanner s = new Scanner(System.in);
        int MyDeg = s.nextInt();

        long res = MyNum;
        for (int i = 2; i != MyDeg + 1; i++) res *= MyNum;
        System.out.println("Number " + MyNum + " to the extent " + MyDeg + " = " + res);
    }
}