/**
* @author Anvar Hasanov
* 11-905
* Task Number 3
*/

import java.util.Scanner;

class Num3 {
    public static void main(String[] args) {
        System.out.println("Ax^2 + Bx + C = 0");
        System.out.println("Enter A, B, C");

        System.out.print("Enter A ---> ");
        Scanner nm1 = new Scanner(System.in);
        double A = nm1.nextDouble();

        System.out.print("Enter B ---> ");
        Scanner nm2 = new Scanner(System.in);
        double B = nm2.nextDouble();

        System.out.print("Enter C ---> ");
        Scanner nm3 = new Scanner(System.in);
        double C = nm3.nextDouble();

        double D = 0, X1 = 0, X2 = 0, res = 0;
        D = (B * B) - 4 * A * C;
        if (D < 0) System.out.println("D < 0, Error");

        else {
            X1 = (-B - Math.sqrt(D)) / 2 * A;
            X2 = (-B + Math.sqrt(D)) / 2 * A;
            System.out.println("X1 = " + X1 + "\n" + "X2 = " + X2);
        }
    }
}