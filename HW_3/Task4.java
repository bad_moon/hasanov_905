/**
* @author Anvar Hasanov
* 11-905
* Task Number 4
*/

import java.util.Scanner;

class Task4 {
	public static void main(String[] args) {
		System.out.println("Enter n");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		n = 2 * n + 1;
		int matrix1[][] = new int[n][n];
		for (int str = 0; str < n; str++) {
			for (int stb = 0; stb < n; stb++) {
				matrix1[str][stb] = 1;
			}
		}
		for (int str = 0; str < (n / 2 + 1); str++) {
			for (int stb = str; stb < (n - str); stb++) {
				matrix1[str][stb] = 0;
			}
		}
		for (int str =(n - 1); str >= (n / 2 + 1); str--) {
			for (int stb = (n - 1 - str); stb <= str; stb++) {
				matrix1[str][stb] = 0;
			}
		}
		for (int str = 0; str < n; str++) {
			for (int stb = 0; stb < n; stb++) {
				System.out.print(matrix1[str][stb] + " ");
			}
			System.out.println();
		}
	}
}