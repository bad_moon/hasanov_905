/**
* @author Anvar Hasanov
* 11-905
* Task Number 3
*/

import java.util.Scanner;

class Task3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int y = sc.nextInt();
		int copyX = x, nX = 0, nRes = 0;
		
		for ( ; copyX != 0; nX++) {
			copyX /= 10;
		}
		int z[] = new int[nX];	
		
		for (int i = 0; x != 0; i++) {
			z[i] = x % 10;
			x /= 10;
		}
        nRes = nX + 1;
		int res[] = new int[nRes];

        for(int i = 0; i < nX; i++) {        
            res[i] = z[i] * y;
		}
		for(int i = 0; i < (nRes - 1); i++) {
                res[i + 1] += (res[i] / 10);
                res[i] %= 10;
        }					
		while(res[(nRes - 1)] == 0) {
                nRes--;
        }
		for(int i = (nRes - 1); i >= 0; i--) {
			System.out.print(res[i]);
		}
	}
}