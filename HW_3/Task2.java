/**
* @author Anvar Hasanov
* 11-905
* Task Number 2
*/

import java.util.Scanner;

class Task2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int y = sc.nextInt();
		int copyX = x, copyY = y;
		int nX = 0, nY = 0, nXandY = 0;
		for ( ; copyX != 0; nX++) {
			copyX /= 10;
		}
		for ( ; copyY != 0; nY++) {
			copyY /= 10;
		}
		int v[] = new int[nY];
		int z[] = new int[nX];	

		for (int i = 0; x != 0; i++) {
			z[i] = x % 10;
			x /= 10;
		}
		for (int i = 0; y != 0; i++) {
			v[i] = y % 10;
			y /= 10;
		}
        nXandY = nX + nY;
		int res[] = new int[nXandY];

        for(int i = 0; i < nX; i++) {
                for(int j = 0; j < nY; j++) {
                        res[i + j] += z[i] * v[j];
				}
		}
		for(int i = 0; i < (nXandY - 1); i++) {
                res[i + 1] += (res[i] / 10);
                res[i] %= 10;
        }					
		while(res[(nXandY - 1)] == 0) {
                nXandY--;
        }
		for(int i = (nXandY - 1); i >= 0; i--) {
			System.out.print(res[i]);
		}
	}
}