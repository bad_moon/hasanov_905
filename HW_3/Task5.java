/**
* @author Anvar Hasanov
* 11-905
* Task Number 5
*/

import  java.util.Scanner;

class Task5 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter matrix size(Example: 3 4 == matrix size 3 x 4)");
		System.out.print("Enter matrix #1 size: ");
		int n1Stb = sc.nextInt();
		int n1Str = sc.nextInt();
		System.out.print("Enter matrix #2 size: ");
		int n2Stb = sc.nextInt();
		int n2Str = sc.nextInt();
		System.out.println();
		
		int matrix1[][] = new int[n1Stb][n1Str];
		for (int i = 0; i < n1Stb; i++) {
			for (int j = 0; j < n1Str; j++) {
				matrix1[i][j] = sc.nextInt();
			}
		}
		System.out.println();
		int matrix2[][] = new int[n2Stb][n2Str];
		for (int i = 0; i < n2Stb; i++) {
			for (int j = 0; j < n2Str; j++) {
				matrix2[i][j] = sc.nextInt();
			}
		}
		System.out.println();
		int resMat = 0;
		int matrixRes[][] = new int[n1Stb][n2Str];
		for (int i = 0; i < n1Stb; i++) {
			for (int j = 0; j < n2Str; j++) {
				resMat = 0;
				for (int a = 0; a < n1Str; a++) {
					resMat += matrix1[i][a] * matrix2[a][j];
				}
					
				matrixRes[i][j] = resMat;
			}
		}
		System.out.println();
		for (int i = 0; i < n1Stb; i++) {
			for (int j = 0; j < n2Str; j++) {
				System.out.print(matrixRes[i][j] + " ");
			}
			System.out.println();
		}
		}
}