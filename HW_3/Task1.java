/**
* @author Anvar Hasanov
* 11-905
* Task Number 1
*/

class Task1 {
	public static void main(String[] args) {
		int n = 0;
		for(; true; n++) {
			try {
				System.out.print(args[n]);
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println();
				break; 
			}
		}
		String argLong = "";
		
		for (int i = 0; i < n; i++) {
			argLong += args[i];
		}
		String newArg = "";
		
			for (int i = (n - 1); i >= 0; i--) {
				newArg += args[i];
		}
		System.out.print(newArg);
	}
}